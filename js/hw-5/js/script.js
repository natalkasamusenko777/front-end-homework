function createNewUser() {
    let newUser = {
        firstName: prompt('Enter you name, please'),
        lastName: prompt('Enter you surname, please'),
        birthday: prompt('Enter the date of your birhday, please (exemple: 10.12.1985)'),
        getLogin: function () {
            return (this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase());

        },
        getAge: function f() {
            let today = new Date();
            let inputDate = +this.birthday.substring(0, 2);
            let inputMonth = +this.birthday.substring(3, 5);
            let inputYear = +this.birthday.substring(6, 10);
            let birthDate = new Date(inputYear,inputMonth-1, inputDate);
            let age = today.getFullYear() - birthDate.getFullYear();
            let m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
            },
        getPassword: function () {
            return (this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6,10));
        },
    };
    return newUser;
}
let newUser = createNewUser();

console.log(newUser);
console.log(`Your age is: ${newUser.getAge()}`);
console.log(`Your password is: ${newUser.getPassword()}`);

