Теоретический вопрос

1. Опишите своими словами как работает цикл forEach

Ответ

Цикл forEach используется для перебора массива. Для каждого элемента массива он вызывает функцию callback. Этой функции он передаёт три параметра callback(item, i, arr):

item – очередной элемент массива
i – его номер
arr – массив, который перебирается

