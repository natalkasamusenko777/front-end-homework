const filterBy = function (arr, type){
    return arr.filter(function(item){
        return typeof item !== type;
    })
};
console.log(filterBy([
    {name: "Nata",
        lastName: "Samusenko",
        age: 27},
    5,
    6,
    "9",
    "Oksana",
    "Roman",
    null,
    true,
], "object"));


