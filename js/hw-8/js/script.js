const input = document.getElementById('number-input');
input.placeholder = 'Price, $';
let quantity = document.getElementById('number-input').value;
let span = document.createElement('span');
let button = document.createElement('button');
button.textContent = 'X';
let p = document.createElement('p');


input.addEventListener('focus', (evt) => {
    evt.target.style.cssText = "border: 3px solid #00FF00";
});

input.addEventListener('blur', (evt) => {
    if (input.value > 0) {
        input.before(p);
        p.before(span);
        span.innerHTML = `Текущая цена: ${quantity}`;
        span.append(button);
        evt.target.style.cssText = "color: #00FF00; border: 1px solid #000000";
    } else {
        evt.target.style.cssText = "border: 3px solid #ff00ff";
        input.after(p);
        p.after(`Please enter correct price`);
        input.value = '';
    }
});

button.addEventListener('click', function () {
    span.remove();
    input.value = '';
});






